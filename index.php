<!DOCTYPE html>
<html>
<head>
	<title>Big Bang Theory</title>
	<link href="assets/css/vendors.css" rel="stylesheet"/>
	<link href="assets/css/app.css" rel="stylesheet"/>
</head>
<body>

<div class="limiter">
	<div class="container-table100">
		<div class="wrap-table100">
			<div class="table100">
				<form class="form" action="./save.php" method="post">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">First name</th>
								<th class="column2">Last name</th>
							</tr>
						</thead>
						<tbody>
								<tr>
									<td class="column1">
										<input type="text" name="people[][firstname]" id="name-0-fname"/>
									</td>
									<td class="column2">
										<input type="text" name="people[][surname]" id="name-0-lname"/>
									</td>
								</tr>
								<tr>
									<td class="column1">
										<input type="text" name="people[][firstname]" id="name-1-fname"/>
									</td>
									<td class="column2">
										<input type="text" name="people[][surname]" id="name-1-lname"/>
									</td>
								</tr>
								<tr>
									<td class="column1">
										<input type="text" name="people[][firstname]" id="name-2-fname"/>
									</td>
									<td class="column2">
										<input type="text" name="people[][surname]" id="name-2-lname"/>
									</td>
								</tr>
								<tr>
									<td class="column1">
										<input type="text" name="people[][firstname]" id="name-3-fname"/>
									</td>
									<td class="column2">
										<input type="text" name="people[][surname]" id="name-3-lname"/>
									</td>
								</tr>
								<tr>
									<td class="column1">
										<input type="text" name="people[][firstname]" id="name-4-fname"/>
									</td>
									<td class="column2">
										<input type="text" name="people[][surname]" id="name-4-lname"/>
									</td>
								</tr>									
						</tbody>
					</table>
					<input type="submit" value="OK" onclick=window.location.href = '/' "/>
				</form>
			</div>
		</div>
	</div>
</div>



<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Big Bang Theory Names</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Thank you for adding the Big Bang Theory names!, you are free to modify the names again.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>

<script type="text/javascript" src="assets/js/vendors.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
</html>