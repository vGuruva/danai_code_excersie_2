<?php

$names = $_POST['people'];
$pairedNames = array_chunk($names, 2);
$nameFile = fopen("./namesdata.txt", "w+") or die("Unable to open file!");
$jsonNames = [];
foreach ($pairedNames as $key => $name):
	$fullName = $name[0]['firstname'] . " " . $name[1]['surname'] . "\n";
	fwrite($nameFile, $fullName);
	array_push($jsonNames,$fullName);
endforeach;

fclose($nameFile);

$jsonFile = fopen('./assets/js/namesdata.json', 'w');
fwrite($jsonFile, json_encode($jsonNames));
fclose($jsonFile);

header("Location: ./");

