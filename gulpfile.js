
var elixir = require('laravel-elixir');
var path = 'app';
elixir.config.assetsPath = 'assets/';

elixir(function (mix) {

    mix.styles([
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/font-awesome/css/font-awesome.min.css'
    ], 'assets/css/vendors.css');

    mix.styles([
        'assets/css/styles.css'
    ], 'assets/css/' + path + '.css');


    mix.scripts([
        'assets/js/scripts.js'
    ], 'assets/js/' + path + '.js');

    mix.scripts([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/jquery-validation/dist/jquery.validate.min.js'
    ], 'assets/js/vendors.js');

});