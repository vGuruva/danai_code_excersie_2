function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile("namesdata.json", function(text){
    var data = JSON.parse(text);
    var splitNames = [];
    var fullName, firstName, lastName;
    
    for (var i = 0; i < data.length; i++) {
        firstName = document.getElementById('name-'+i+'-fname');
        lastName = document.getElementById('name-'+i+'-lname');
        fullName = data[i].split(" ");
        firstName.value = fullName[0];
        lastName.value = fullName[1];
    }

});
