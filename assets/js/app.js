function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile("./assets/js/namesdata.json", function(text){
    var data = JSON.parse(text);
    var splitNames = [];
    var fullName, firstName, lastName;
    
    for (var i = 0; i < data.length; i++) {
        firstName = document.getElementById('name-'+i+'-fname');
        lastName = document.getElementById('name-'+i+'-lname');
        fullName = data[i].split(" ");
        firstName.value = fullName[0];
        lastName.value = fullName[1];
    }

});

var enquiryValidator = $(".form").validate({
        ignore: [],
        rules: {
            'people[][surname]': {
                required: true,
                minlength: 2,
                maxlength: 160,
                normalizer: function(value) {
                    return $.trim(value);
                },

            },
            'people[][firstname]': {
                required: true,
                minlength: 2,
                maxlength: 160,
                normalizer: function(value) {
                    return $.trim(value);
                },

            },
        },
        messages:{
            'people[][firstname]':{
                required: "Please enter first name.",
                minlength: "Please enter at least 2 characters.",
                maxlength: "Please enter at least 160 characters."
            },

            'people[][surname]':{
                required: "Please enter surname.",
                minlength: "Please enter at least 2 characters.",
                maxlength: "Please enter at least 160 characters."
            },
        }
});


$('.form').submit(function(e) {
    e.preventDefault();
    $('.modal').modal('show');
    return false;
});

//# sourceMappingURL=app.js.map
